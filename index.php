<?php

require_once __DIR__ . '/vendor/autoload.php';

use Overload\Color;
use Overload\Exceptions\UserException;
use Overload\User;

try {
    $user = new User('Serhii', 33, 'example@gamail.com');
//    $user->setName('serhii');
    var_dump($user->getAll());
//    $color = new Color(100, 100, 100);
//    $color2 = new Color(200, 200, 200);
//    $resulEquals = $color->equals($color2);
//    var_dump($resulEquals);
//
//    $randomColor = Color::random();
//    var_dump($randomColor);
//
//    $color3 = new Color(250, 250, 250);
//    $mixedColor = $color3->mix(new Color(100, 100, 100));
//    var_dump($mixedColor->getRed());
//    var_dump($mixedColor->getGreen());
} catch (UserException $exception) {
    echo $exception->getMessage() . PHP_EOL;
} catch (\Exception $exception) {
    echo $exception->getMessage() . PHP_EOL;
}