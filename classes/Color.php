<?php

declare(strict_types=1);

namespace Overload;

class Color
{
    use ValidationColor;

    private int $red;
    private int $green;
    private int $blue;

    public function __construct(int $red, int $green, int $blue)
    {
        $this->setRed($red);
        $this->setGreen($green);
        $this->setBlue($blue);
    }

    public function getRed(): int
    {
        return $this->red;
    }

    public function setRed(int $red): void
    {
        $this->validateColor('red', $red);

        $this->red = $red;
    }

    public function getGreen(): int
    {
        return $this->green;
    }

    public function setGreen(int $green): void
    {
        $this->validateColor('green', $green);

        $this->green = $green;
    }

    public function getBlue(): int
    {
        return $this->green;
    }

    public function setBlue(int $blue): void
    {
        $this->validateColor('blue', $blue);

        $this->blue = $blue;
    }

    public function equals(Color $obj): bool
    {
        return $this->green === $obj->green && $this->red === $obj->red && $this->blue === $obj->blue;
    }

    public static function random(): Color
    {
        $red = rand(0, 255);
        $green = rand(0, 255);
        $blue = rand(0, 255);
        return new self($red, $green, $blue);
    }

    public function mix(Color $obj): Color
    {
        if (!isset($this->red) || !isset($this->blue) || !isset($this->green)) {
            throw new \Exception("colors must be set");
        }

        $this->setRed(($this->red + $obj->getRed()) / 2);
        $this->setGreen(($this->green + $obj->getGreen()) / 2);
        $this->setBlue(($this->blue + $obj->getBlue()) / 2);

        return $this;
    }
}