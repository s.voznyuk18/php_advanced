<?php

declare(strict_types=1);

namespace Overload\Exceptions;

use RuntimeException;

class UserException extends RuntimeException
{
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}