<?php

declare(strict_types=1);

namespace Overload;

use Overload\Exceptions\UserException;

class User
{
    private string $name;
    private int $age;
    private string $email;

    public function __construct(string $name, int $age, string $email)
    {
        $this->setName($name);
        $this->setAge($age);
        $this->setEmail($email);
    }

    private function setName(string $name): void
    {
        $this->name = $name;
    }

    private function getName(): string
    {
        return $this->name;
    }

    private function setAge(int $age): void
    {
        $this->age = $age;
    }

    private function getAge(): int
    {
        return $this->age;
    }

    private function setEmail(string $email): void
    {
        $this->email = $email;
    }

    private function getEmail(): string
    {
        return $this->email;
    }

    public function getAll(): string
    {
        return "name: $this->name; age: $this->age; email: $this->email";
    }

    public function __call(string $name, array $arguments): mixed
    {
        throw new UserException("$name method is not exist");
    }
}