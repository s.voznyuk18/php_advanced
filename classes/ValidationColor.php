<?php

declare(strict_types=1);

namespace Overload;

trait ValidationColor
{
    private function validateColor(string $colorName, int $value): void
    {
        if ($value < 0 || $value > 255) {
            throw new \Exception("$colorName is invalid color");
        }
    }
}